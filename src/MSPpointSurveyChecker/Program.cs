﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace MSPpointSurveyChecker
{
    class Program
    {
        public static int CulpritCount = 0;
        public static bool WebLinkCheck(Points po)
        {
            WebClient client = new WebClient();

            try
            {
                string Html = client.DownloadString(po.Url);

                if (Html.Contains("ErrorText") || Html.Contains("errorContent"))
                {
                    Console.WriteLine(CulpritCount++);
                    Console.WriteLine("\n\n\n\nCulpritLink : " + po.Url);
                    Console.WriteLine("Culprit Name : " + po.Name);
                    return false;
                }
                else return true;
            }
            catch (Exception ex)
            {
                if (ex.Message == "The remote server returned an error: (403) Forbidden.")
                {
                    return false;
                }
                else
                    return false;
            }
        }
        static void Main(string[] args)
        {
            string Url = @"D:\MSPpointSurveyChecker\AUST.xlsx";
            string Url2 = @"D:\MSPpointSurveyChecker\AUST.txt";
            string Url3 = @"D:\MSPpointSurveyChecker\AUSTCulprit.txt";

            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            string str;
            int rCnt = 0;
            int cCnt = 0;

            xlApp = new Excel.Application();
            //xlWorkBook = xlApp.Workbooks.Open("csharp.net-informations.xls", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkBook = xlApp.Workbooks.Open(Url, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            range = xlWorkSheet.UsedRange;
            int CulpritCount = 1;

            List<Points> AllMspsPoint = new List<Points>();
            List<Points> CulpritMspsPoint = new List<Points>();

            for (rCnt = 2; rCnt <= range.Rows.Count; rCnt++)
            {
                //for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
                //{
                //    str = (range.Cells[rCnt, cCnt] as Excel.Range).Value2.ToString();
                //    Console.WriteLine(str);
                //}
                Points MspPoint = new Points();
                MspPoint.ActivityType = new Activity();

                MspPoint.Name = (range.Cells[rCnt, 1] as Excel.Range).Value2.ToString();
                MspPoint.Email = (range.Cells[rCnt, 2] as Excel.Range).Value2.ToString();
                MspPoint.ActivityType.ActivityName = (range.Cells[rCnt, 3] as Excel.Range).Value2.ToString();
                MspPoint.ActivityType.SetActivitymark();
                MspPoint.Url = (range.Cells[rCnt, 4] as Excel.Range).Value2.ToString();
                MspPoint.Date = (range.Cells[rCnt, 5] as Excel.Range).Value;
                if (MspPoint.Url.Contains(','))
                {

                    var count = MspPoint.Url.Where(x => x == ',').Count();
                    //Console.WriteLine("Culprit found !!!");
                    //Console.WriteLine("Comma Repeated in Url total : " + count.ToString());
                    //Console.WriteLine(MspPoint.Name);
                    //Console.WriteLine(MspPoint.Url);
                    //Console.WriteLine("Row : " + rCnt);
                    //Console.WriteLine("\t\t\t\t\n\n\n\n\t\t\t\t\n\n\n\n" + CulpritCount++);

                    Points MspPoint2 = new Points();
                    MspPoint2.ActivityType = new Activity();

                    MspPoint2.Name = (range.Cells[rCnt, 1] as Excel.Range).Value2.ToString();
                    MspPoint2.Email = (range.Cells[rCnt, 2] as Excel.Range).Value2.ToString();
                    MspPoint2.ActivityType.ActivityName = (range.Cells[rCnt, 3] as Excel.Range).Value2.ToString();
                    MspPoint2.ActivityType.SetActivitymark();

                    int cutoffPoint = MspPoint.Url.IndexOf(',');
                    MspPoint2.Url = MspPoint.Url.Substring(0, cutoffPoint);
                    MspPoint.Url = MspPoint.Url.Substring(cutoffPoint + 1);

                    if (WebLinkCheck(MspPoint2))
                    {
                        AllMspsPoint.Add(MspPoint2);
                    }
                    else
                        CulpritMspsPoint.Add(MspPoint2);
                }
                else if (MspPoint.Url.Contains(';'))
                {
                    var count = MspPoint.Url.Where(x => x == ';').Count();
                    //Console.WriteLine("Culprit found !!!");
                    //Console.WriteLine("Comma Repeated in Url total : " + count.ToString());
                    //Console.WriteLine(MspPoint.Name);
                    //Console.WriteLine(MspPoint.Url);
                    //Console.WriteLine("Row : " + rCnt);
                    //Console.WriteLine("\t\t\t\t\n\n\n\n\t\t\t\t\n\n\n\n" + CulpritCount++);

                    Points MspPoint2 = new Points();
                    MspPoint2.ActivityType = new Activity();

                    MspPoint2.Name = (range.Cells[rCnt, 1] as Excel.Range).Value2.ToString();
                    MspPoint2.Email = (range.Cells[rCnt, 2] as Excel.Range).Value2.ToString();
                    MspPoint2.ActivityType.ActivityName = (range.Cells[rCnt, 3] as Excel.Range).Value2.ToString();
                    MspPoint2.ActivityType.SetActivitymark();

                    int cutoffPoint = MspPoint.Url.IndexOf(';');
                    MspPoint2.Url = MspPoint.Url.Substring(0, cutoffPoint);
                    MspPoint.Url = MspPoint.Url.Substring(cutoffPoint + 1);

                    if (WebLinkCheck(MspPoint2))
                    {
                        AllMspsPoint.Add(MspPoint2);
                    }
                    else
                        CulpritMspsPoint.Add(MspPoint2);
                }
                else
                {
                    if (WebLinkCheck(MspPoint))
                    {
                        AllMspsPoint.Add(MspPoint);
                    }
                    else
                        CulpritMspsPoint.Add(MspPoint);
                }
                    

                //Console.WriteLine("\t\t\t\t\n\n\n\nOne Row Printed\t\t\t\t\n\n\n\n");
                
            }

            
            string json = JsonConvert.SerializeObject(AllMspsPoint);
            File.WriteAllText(Url2, json);

            string json2 = JsonConvert.SerializeObject(CulpritMspsPoint);
            File.WriteAllText(Url3, json2);

            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            Console.ReadLine();
        }
        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Unable to release the Object " + ex.ToString());
                Console.WriteLine("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        } 
    }
}
