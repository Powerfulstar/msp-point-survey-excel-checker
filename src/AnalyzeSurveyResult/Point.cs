﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyzeSurveyResult
{
    public class Person
    {
        public string Name { get; set; }
        public int TotalPoint { get; set; }
        public string Email { get; set; }
    }
    public class Points
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Activity ActivityType { get; set; }
        public string Url { get; set; }
        public bool IsLinkWorking { get; set; }
        public DateTime Date { get; set; }
    }
    public class Activity
    {
        public string ActivityName { get; set; }
        public int Activitymark { get; set; }
        public List<ActivityMark> ActivityList { get; set; }

        public Activity()
        {
            ActivityList = new List<ActivityMark>();
            ActivityList.Add(new ActivityMark() { ActivityName = "Above & Beyond (Points case by case basis)", Mark = 0 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Attendance at an event (1 pt)", Mark = 1 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Blogging/Social media activity (1 pt)", Mark = 1 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Each App Studio app (2 pts)", Mark = 2 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Each App Studio Universal App (5 pts)", Mark = 5 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Each news coverage of MSPs (15 pts)", Mark = 15 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Each Video Tutorial (3 pts)", Mark = 3 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Each Visual Studio app (5 pts)", Mark = 5 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Each Visual Studio Universal App (7 pts)", Mark = 7 });
            ActivityList.Add(new ActivityMark() { ActivityName = "Speaker at an event (10 pts)", Mark = 10 });
        }
        public void SetActivitymark()
        {
            foreach (var item in ActivityList)
            {
                if (ActivityName == item.ActivityName)
                {
                    Activitymark = item.Mark;
                }
            }
        }
    }
    public class ActivityMark
    {
        public int Mark { get; set; }
        public string ActivityName { get; set; }
    }
}
