﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyzeSurveyResult
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Points> AllMspsPoint = new List<Points>();
            string Url2 = @"D:\MSPpointSurveyChecker\AUST.txt";
            string Url3 = @"D:\MSPpointSurveyChecker\AUSTMSPRESULT.txt";
            string Json = File.ReadAllText(Url2);
            AllMspsPoint = JsonConvert.DeserializeObject<List<Points>>(Json);


            #region Url Uniq

            int i = 0;
            List<string> UniqUrl = new List<string>();

            List<Points> AllMspsPoint2 = new List<Points>();
            foreach (var item in AllMspsPoint.Select(o => o.Url).Distinct())
            {
                UniqUrl.Add(item);
                i++;
            }

            foreach (var item in UniqUrl)
            {
                foreach (var item2 in AllMspsPoint)
                {
                    if (item == item2.Url)
                    {
                        AllMspsPoint2.Add(item2);
                        break;
                    }
                }
            }

            Json = JsonConvert.SerializeObject(AllMspsPoint2);


            Console.WriteLine(i);
            Console.WriteLine(AllMspsPoint.Count);
            Console.WriteLine(AllMspsPoint2.Count);
            Console.ReadLine();
            #endregion


            #region Name Uniq

            List<string> UniqName = new List<string>();
            List<Person> MSPList = new List<Person>();
            i = 0;
            foreach (var item in AllMspsPoint2.Select(o => o.Name).Distinct())
            {
                UniqName.Add(item);
                i++;
            }

            foreach (var item in UniqName)
            {
                Person MSP = new Person();
                foreach (var item2 in AllMspsPoint2)
                {                    
                    if (item == item2.Name)
                    {                        
                        MSP.Name = item;
                        MSP.Email = item2.Email;
                        MSP.TotalPoint += item2.ActivityType.Activitymark;
                    }
                }
                MSPList.Add(MSP);
            }

            Json = JsonConvert.SerializeObject(MSPList);
            File.WriteAllText(Url3, Json);

            Console.WriteLine("MSP LIST : " + MSPList.Count);
            Console.WriteLine("UNiq Name : " + i);
            Console.ReadLine();
        }
            #endregion
          
    }
}
